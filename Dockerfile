FROM node:lts-buster-slim AS base
LABEL maintainer="Jon Pavelich <docker@jonpavelich.com>"
WORKDIR /usr/src/app
COPY package*.json ./

FROM base AS builder
RUN npm ci
COPY . .
RUN npm run build

FROM base AS final
RUN npm ci --only=production
COPY README.md settings.json ./
COPY --from=builder /usr/src/app/dist/ .
ENV NODE_ENV=production
EXPOSE 3000
CMD ["node", "--require", "module-alias/register", "./start.js"]
