# Welcome to TypeSnake

A BattleSnake AI and snake management suite.

## Team

Jon Pavelich ([ 🦊GitLab](https://gitlab.com/jonpavelich), [GitHub](https://github.com/jonpavelich) )  
Tyler Harnadek ([ 🦊GitLab](https://gitlab.com/tharnadek), [GitHub](https://github.com/tharnadek) )

## Getting Started

1. Clone the Repo
2. `npm install`
3. `npm run start-dev`
4. Go to localhost:3000

## 🏗 Available Scripts

-   Run the server in development mode: `npm run start-dev`.
-   Run all unit-tests: `npm test`.
-   Run a single unit-test: `npm test -- "name of test file (i.e. Users)"`.
-   Check for linting errors: `npm run lint`.
-   Build the project for production: `npm run build`.
-   Run the production build: `npm start`.
-   Create docs: `npm run docs`.

## Strategy

### Targeting

This snake chooses targets based on its desired length and hunger level; currently it can choose between food and its own tail. When selecting between multiple targets, it currently chooses the closest one computed by Manhattan distance.

### Board Weighting

The board is weighted according to how favourable (positive weighting) or dangerous (negative weighting) the tile is. The weights are initialized to zero. Currently, the area around the heads of snakes who are smaller than us is positively weighted, and the area around the heads of snakes who are the same length or larger than us is negatively weighted, and snake bodies are very negatively weighted.

### Pathfinding

Pathfinding is done using A\*. The board weighting of each tile is subtracted from G(n) (cost to travel from start to n), which discourages negative weighted tiles (by increasing the cost to reach them) and encourages positive tiles (by decreasing the cost to reach them). The heuristic function simply computes Manhattan distance to guide the search. This finds an path which attempts to pass through positively weighted tiles near our path and avoids negatively weighted tiles.

## Issues

For a list of bugs and improvements which are needed for future contests, see the [Issue Tracker](https://gitlab.com/jonpavelich/battlesnake/issues).

## License

This code is available under the GNU Affero General Public License v3.0. See [LICENSE.md](LICENSE.md).

Please note that the terms of this license **do not** permit you to run this application on a server without also providing the original attributions and full source code as part of the running application, regardless of any modifications you may make. If you're looking for a snake which you may modify and host as your own freely, please check out our [Python 3 Starter Snake](https://github.com/jonpavelich/battlesnake-python3-starter) which is available under the much more permissive MIT License.
