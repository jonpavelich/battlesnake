import { ICoord } from '@entities';

/**
 * Object used to represent snake, primarily made up of simple coordinates
 */
export interface ISnake {
    id: string;
    name: string;
    /** health of 100 can indicate that snake ate on last turn */
    health: number;
    /** Includes head and tail */
    body: ICoord[];
}

/**
 * Object used to represent snake with helper methods
 */
export interface IBattleSnake extends ISnake {
    head(): ICoord;
    tail(): ICoord;
    length(): number;
}

export class BattleSnake implements IBattleSnake {
    id: string;
    name: string;
    health: number;
    body: ICoord[];

    constructor({ id, name, health, body }: { id: string; name: string; health: number; body: ICoord[] }) {
        this.id = id;
        this.name = name;
        this.health = health;
        this.body = body;
    }
    head(): ICoord {
        return this.body[0];
    }
    tail(): ICoord {
        return this.body[this.body.length - 1];
    }
    length(): number {
        return this.body.length;
    }
}
