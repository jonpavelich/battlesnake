import { ISnake } from '@entities';
import { BattleSnake } from './Snake';

/** contains most relevant details for game logic */
export interface IBoard {
    /** number of board rows */
    height: number;
    /** number of board columns */
    width: number;
    /** snake consumes these when its head passes over them */
    food: ICoord[];
    /** your snake is included in the following array */
    snakes: ISnake[];

    isInbounds({ x, y }: { x: number; y: number }): boolean;
    getSurrounding({ x, y }: { x: number; y: number }): ICoord[];
    getAdjacent({ x, y }: { x: number; y: number }): ICoord[];
}

/**
 * Coordinate used throughout the game system
 */
export interface ICoord {
    x: number;
    y: number;
}

export class Board implements IBoard {
    height: number;
    width: number;
    food: ICoord[];
    snakes: BattleSnake[];

    constructor({ height, width, food, snakes }: { height: number; width: number; food: ICoord[]; snakes: ISnake[] }) {
        this.height = height;
        this.width = width;
        this.food = food;
        this.snakes = snakes.map(snake => new BattleSnake({ ...snake }));
    }
    /** Checks if a coordinate is inbounds according to the size of the board */
    isInbounds({ x, y }: { x: number; y: number }): boolean {
        return x >= 0 && y >= 0 && y < this.height && x < this.width;
    }

    /** returns all inbound adjacent coords excluding the center */
    getSurrounding(coord: ICoord): ICoord[] {
        /* make sure this is inbounds */
        const { x, y } = coord;
        if (!this.isInbounds({ x, y })) return [];
        /* store results in array */
        const surroundings: ICoord[] = [];
        for (let i = -1; i <= 1; i++) {
            for (let j = -1; j <= 1; j++) {
                const newCoord = { x: x + i, y: y + j };
                /* do not include the center coordinate */
                if (this.isInbounds(newCoord) && !(newCoord.x === x && newCoord.y === y)) {
                    surroundings.push(newCoord);
                }
            }
        }
        return surroundings;
    }

    /** returns all inbound orthagonally adjacent coords */
    getAdjacent(coord: ICoord): ICoord[] {
        /* make sure this is inbounds */
        const { x, y } = coord;
        if (!this.isInbounds({ x, y })) return [];
        /* store results in array */
        const surroundings: ICoord[] = [
            { x: x, y: y - 1 }, // up
            { x: x, y: y + 1 }, // down
            { x: x - 1, y: y }, // left
            { x: x + 1, y: y }, // right
        ];
        return surroundings.filter(value => {
            return this.isInbounds(value);
        });
    }
}
