/**
 * Enumerates possible board directions
 */
export enum Directions {
    Left = 'left',
    Right = 'right',
    Up = 'up',
    Down = 'down',
}

type TDirections = Directions.Left | Directions.Right | Directions.Up | Directions.Down;

/**
 * Represents the response object sent to the battlesnake servers
 * MoveResponses created with no arguments will be randomly selected
 */
export interface IMoveResponse {
    move: TDirections;
}

export class MoveResponse implements IMoveResponse {
    public move: TDirections;

    constructor(move?: TDirections) {
        if (move) {
            this.move = move;
        } else {
            const moves = [Directions.Left, Directions.Right, Directions.Up, Directions.Down];
            this.move = moves[Math.floor(Math.random() * 4)];
        }
    }
}
