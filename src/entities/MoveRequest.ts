import { IBoard, ISnake, BattleSnake, Board } from '@entities';
/**
 * Represents the response object sent by the battlesnake servers
 */

export interface IMoveRequest {
    game: IGame;
    turn: number;
    board: IBoard;
    you: BattleSnake;
}

/** contains game properties */
export interface IGame {
    id: string;
}

export class MoveRequest implements IMoveRequest {
    game: IGame;
    turn: number;
    board: IBoard;
    you: BattleSnake;

    constructor({ game, turn, board, you }: { game: IGame; turn: number; board: IBoard; you: ISnake }) {
        this.game = game;
        this.turn = turn;
        this.board = board = new Board({ ...board });
        this.you = new BattleSnake({ ...you });
    }
}
