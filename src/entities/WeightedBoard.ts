import { Board, IBoard } from '@entities';
import { ICoord } from './Board';
import { ISnake } from './Snake';

/** contains most relevant details for game logic */
export interface IWeightedBoard extends IBoard {
    /** your snake is included in the following array */
    weights: number[][];
    /** */
    applyWeights({ coords, weight }: { coords: ICoord[]; weight: number }): void;
}

export class WeightedBoard extends Board {
    weights: number[][];

    constructor({
        height,
        width,
        food,
        snakes,
        weights,
    }: {
        height: number;
        width: number;
        food: ICoord[];
        snakes: ISnake[];
        weights?: number[][];
    }) {
        super({ height, width, food, snakes });
        if (weights) {
            this.weights = weights;
        } else {
            this.weights = Array(height)
                .fill(0)
                .map(() => Array(width).fill(0));
        }
    }
    /** applyWeights applies weights to a set of coordinates.
     * It receives coords instead of a weighted matrix so that it does not need to traverse
     */
    applyWeights({ coords, weight }: { coords: ICoord[]; weight: number }): void {
        coords.forEach((coord: ICoord) => {
            const { x, y } = coord;
            this.weights[x][y] += weight;
        });
    }

    toString(): string {
        let weights = '        ';
        for (let i = 0; i < this.height; i++) {
            for (let j = 0; j < this.width; j++) {
                const w = this.weights[j][i].toString().padStart(4);
                weights = weights.concat(w, ',');
            }
            weights = weights.concat('\n        ');
        }

        return `Weighted Board -- height: ${this.height} width: ${this.width} 
        food: ${JSON.stringify(this.food)} 
        snakes: ${JSON.stringify(this.snakes)}
        weights: \n${weights}\n`;
    }
}
