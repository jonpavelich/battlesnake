/**
 * Setup the winston logger.
 *
 * Documentation: https://github.com/winstonjs/winston
 */

import { createLogger, format, transports } from 'winston';
import httpContext from 'express-http-context';

// Import functions
const { Console, File } = transports;
const { printf } = format;

// Add game and snake IDs to the logs when available
const addGameIds = format(info => {
    info.game = httpContext.get('game') || 'no game id';
    info.snake = httpContext.get('snake') || 'no snake id';
    return info;
});

// Base format: add timestamps and game IDs
const baseFormat = format.combine(format.timestamp(), addGameIds());

// File format: base format rendered to JSON
const fileFormat = format.combine(baseFormat, format.json());

// Console format: base format rendered as a string ("timestamp level (gameId, snakeId): message")
// When an object is logged instead of a string, extra props are appended to message
const consoleFormat = format.combine(
    baseFormat,
    format.colorize(),
    printf(info => {
        const knownProps = ['message', 'level', 'timestamp', 'game', 'snake', 'stack'];
        const extraProps = Object.keys(info)
            .filter(key => !knownProps.includes(key))
            .map(key => [key, info[key]]);
        let message = info.stack ? info.stack : info.message;
        if (extraProps.length > 0) {
            message = `${message} (${JSON.stringify(Object.fromEntries(extraProps))})`;
        }
        return `${info.timestamp} ${info.level} (${info.game}, ${info.snake}): ${message}`;
    }),
);

// Initialize logger
const winstonLogger = createLogger({
    level: 'debug',
});

/* In production: write JSON log files and print errors to the console
 * Everywhere else: print everything to the console
 *
 * env.LOG_LEVEL is used to set the maximum log level and defaults to info
 */
if (process.env.NODE_ENV === 'production') {
    const errorFileTransport = new File({
        filename: './logs/error.log',
        format: fileFormat,
        level: 'error',
    });
    const combinedFileTransport = new File({
        filename: './logs/combined.log',
        format: fileFormat,
        level: process.env.LOG_LEVEL || 'info',
    });
    const consoleTransport = new Console({
        format: consoleFormat,
        level: 'error',
    });
    winstonLogger.add(errorFileTransport);
    winstonLogger.add(combinedFileTransport);
    winstonLogger.add(consoleTransport);
} else {
    const consoleTransport = new Console({
        format: consoleFormat,
        level: process.env.LOG_LEVEL || 'info',
    });
    winstonLogger.add(consoleTransport);
}

// Export logger
export const logger = winstonLogger;
