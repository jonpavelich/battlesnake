/* eslint-disable @typescript-eslint/no-explicit-any */
import jsonfile from 'jsonfile';

// TODO: see https://www.npmjs.com/package/jsonpath

export class Settings {
    private settings: { [key: string]: any };

    constructor(filename = 'settings.json') {
        this.settings = jsonfile.readFileSync(filename);
    }

    // TODO: Can the type handling be fixed to not use any? Values are actually 'string|object'.
    public get(key: string): any {
        let value: any = this.settings;
        let subkey = key;
        while (subkey.includes('.')) {
            const prefix = subkey.split('.', 1)[0];
            if (!value[prefix]) {
                throw new Error('Key does not exist');
            }
            value = value[prefix];
            subkey = subkey.substring(prefix.length + 1);
        }
        return value[subkey];
    }
}
