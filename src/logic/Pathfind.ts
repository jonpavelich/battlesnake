import { ICoord, WeightedBoard, MoveResponse, Directions } from '@entities';
import { Settings } from '@shared';
import { logger } from '@shared';
import PriorityQueue from 'ts-priority-queue';

interface INode {
    weight: number;
    neighbours: number[];
}

interface IGraph {
    [key: number]: INode;
}

type NumberMap = { [key: number]: number };

/* Find an optimal path from source to one of the targets
 *
 * @param source: the current position to pathfind from
 * @param targets: a list of target coordinates to consider
 * @param board: the weighted board
 * @returns: a MoveResponse indicating the direction to move
 */
export function pathfind(source: ICoord, targets: ICoord[], board: WeightedBoard): MoveResponse {
    logger.debug({ message: 'Starting pathfinding', source, targets });
    const path = aStar(source, targets, board).map(index => {
        return indexToCoord(index, board.width);
    });
    logger.debug({ message: 'Finished pathfinding', path });

    const nextCoord = path[1];
    let direction: Directions;
    try {
        direction = directionToNextCoord(source, nextCoord);
    } catch (e) {
        logger.error('Pathfinding returned an invalid path');
        throw e;
    }
    return new MoveResponse(direction);
}

/* Find the direction we need to move to get from current to next
 *
 * @param current: the coordinate to move from
 * @param next: the coordinate to move to
 * @returns: the direction to move
 * @throws Error if the coordiates are not adjacent
 */
export function directionToNextCoord(current: ICoord, next: ICoord): Directions {
    const deltaX = next.x - current.x;
    const deltaY = next.y - current.y;
    let direction: Directions;
    if (deltaX == 0 && deltaY == -1) {
        direction = Directions.Up;
    } else if (deltaX == 0 && deltaY == 1) {
        direction = Directions.Down;
    } else if (deltaX == -1 && deltaY == 0) {
        direction = Directions.Left;
    } else if (deltaX == 1 && deltaY == 0) {
        direction = Directions.Right;
    } else {
        throw new Error(`It is impossible to move from ${current} to ${next} in one step`);
    }
    return direction;
}

/* Run the A-star pathfinding algorithm to find an optimal path.
 *
 * When multiple targets exist the heuristic targets the closest one,
 * but cost is still considered, so a cheap target far away should be
 * chosen over a very costly close one
 */
function aStar(source: ICoord, targets: ICoord[], board: WeightedBoard): number[] {
    if (targets.length < 1) {
        throw new Error('Cannot pathfind without a target');
    }

    const graph = graphify(board);

    const start = coordToIndex(source, board.width);
    const goals = targets.map(coord => {
        return coordToIndex(coord, board.width);
    });

    const gScore: NumberMap = {};
    const fScore: NumberMap = {};
    for (const key in graph) {
        gScore[key] = global.Infinity;
        fScore[key] = global.Infinity;
    }
    gScore[start] = 0;
    fScore[start] = h(source, targets);

    /* We maintain both a Set and PriorityQueue, because PriorityQueue does not allow membership checks */
    const openSet = new Set<number>([start]);
    const openSetQueue = new PriorityQueue({
        comparator: (a: number, b: number): number => fScore[a] - fScore[b],
        initialValues: [start],
    });

    /* previously evaluated nodes */
    const closedSet = new Set<number>();

    const cameFrom: NumberMap = {};

    // while open set is not empty
    while (openSet.size > 0) {
        // current = node in openSet with lowest fScore
        const current = openSetQueue.dequeue();
        // if current == goal
        if (goals.includes(current)) {
            return reconstructPath(cameFrom, current);
        }
        openSet.delete(current);
        closedSet.add(current);

        graph[current].neighbours.forEach(neighbour => {
            if (!closedSet.has(neighbour)) {
                const tentativeGScore = gScore[current] + graph[neighbour].weight;
                if (tentativeGScore < gScore[neighbour]) {
                    cameFrom[neighbour] = current;
                    gScore[neighbour] = tentativeGScore;
                    fScore[neighbour] = gScore[neighbour] + h(indexToCoord(neighbour, board.width), targets);
                    if (!openSet.has(neighbour)) {
                        openSet.add(neighbour);
                        openSetQueue.queue(neighbour);
                    }
                }
            }
        });
    }

    logger.error('Unable to find a path to any target');
    return [];
}

/* Reconstructs the optimal path found by aStar */
function reconstructPath(cameFrom: NumberMap, current: number): number[] {
    const path = [current];
    const keys: number[] = [];
    for (const key in cameFrom) {
        keys.push(parseInt(key));
    }
    while (keys.indexOf(current) > -1) {
        current = cameFrom[current];
        path.push(current);
    }

    return path.reverse();
}

/* Heuristic costing function for A* pathfinding
 *
 * @returns the minimum Manhattan distance from the source to any target
 */
function h(source: ICoord, destinations: ICoord[]): number {
    if (destinations.length < 1) {
        logger.warn('Heuristic function was not provided with any destinations');
    }
    let minCost = global.Infinity;
    for (const dest of destinations) {
        const deltaX = Math.abs(source.x - dest.x);
        const deltaY = Math.abs(source.y - dest.y);
        minCost = Math.min(minCost, deltaX + deltaY);
    }
    return minCost;
}

/* Build a traversable weighted graph (as an adjacency list) from a WeightedBoard
 *
 * Coordinates with (weight <= threshold) are treated as walls (absent from graph)
 *
 * @param board: the WeightedBoard to convert
 * @returns a weighted graph constructed from the board
 */
function graphify(board: WeightedBoard): IGraph {
    const settings = new Settings();
    const threshold = settings.get('weights.illegal');

    const graph: IGraph = {};
    for (let i = 0; i < board.width; i++) {
        for (let j = 0; j < board.height; j++) {
            const coord = { x: i, y: j };
            const weight = board.weights[i][j];
            if (weight > threshold) {
                const index = coordToIndex(coord, board.width);
                const neighbours = board
                    .getAdjacent(coord)
                    .filter(value => {
                        return board.weights[value.x][value.y] > threshold;
                    })
                    .map(value => {
                        return coordToIndex(value, board.width);
                    });
                graph[index] = { weight: weight * -1, neighbours };
            }
        }
    }
    return graph;
}

/* Convert an ICoord in to a numeric INode index */
function coordToIndex(coord: ICoord, width: number): number {
    return coord.y * width + coord.x;
}

/* Convert a numeric INode index back to an ICoord */
function indexToCoord(index: number, width: number): ICoord {
    return { x: index % width, y: Math.floor(index / width) };
}
