import { IBattleSnake, MoveRequest, MoveResponse, WeightedBoard } from '@entities';
import { logger, Settings } from '@shared';
import { pathfind } from './Pathfind';

const settings = new Settings();

/**
 * Consumes a MoveRequest and returns a MoveResponse
 * This function forms the root of the snake's logic.
 */
export function getMove(req: MoveRequest): MoveResponse {
    const weightedBoard = weightBoard(new WeightedBoard(req.board), req.you);
    let targets = [];
    if (req.you.health < settings.get('desired.health')) {
        logger.verbose("Time to eat, I'm starving");
        targets.push(...weightedBoard.food);
    } else if (req.you.length() < settings.get('desired.length')) {
        logger.verbose('Time to eat, I want to grow');
        targets.push(...weightedBoard.food);
    }

    if (targets.length == 0) {
        logger.verbose('I wonder if I can catch my tail?');
        if (req.you.health != 100) {
            targets.push(req.you.tail());
        } else {
            logger.verbose('Better be careful, my tail is about to grow.');
            // TODO: Fix this behaviour
            logger.warn("I don't know how to avoid my tail yet, guess I'll bite it!");
            targets.push(req.you.tail());
        }
    }

    // Remove targets if they are under the snake's head
    targets = targets.filter(coord => coord.x != req.you.head().x || coord.y != req.you.head().y);

    logger.debug({ message: 'Finished computing weighted board', board: weightedBoard });
    const move = pathfind(req.you.head(), targets, weightedBoard);
    logger.info(`Moving ${move.move}`);
    return move;
}

function weightBoard(board: WeightedBoard, my: IBattleSnake): WeightedBoard {
    const settings = new Settings();

    const bodyWeight = parseInt(settings.get('weights.illegal'));
    const headWeight = parseInt(settings.get('weights.head'));
    const foodWeight = parseInt(settings.get('weights.food'));

    /* Weight the bodies of all snakes on the board.
     * The tail segment is weighted only if the snake has 100 health,
     * which *should* indicate that the snake has just eaten.
     */
    board.snakes.forEach(snake => {
        let body = snake.body.slice();
        /* remove tail segment if the snake didn't eat - we don't need to weight it */
        if (snake.health != 100) {
            body.pop();
        }
        /* remove head segment(s) if this is us - pathfinding doesn't like it when you start inside a wall */
        if (snake.id == my.id) {
            const head = snake.head();
            body = body.filter(val => val.x !== head.x || val.y !== head.y);
        }
        board.applyWeights({ coords: body, weight: bodyWeight });

        /* avoid snake heads */
        if (snake.id != my.id) {
            const surroundingCoords = board.getSurrounding(snake.head());
            let weight = 0;
            if (snake.length() >= my.length()) {
                weight = -headWeight;
            } else {
                weight = headWeight;
            }
            board.applyWeights({ coords: surroundingCoords, weight });
        }
    });
    board.applyWeights({ coords: board.food, weight: foodWeight });

    return new WeightedBoard({
        ...board,
    });
}
