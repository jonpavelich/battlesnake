import express from 'express';
import morgan from 'morgan';
import { logger } from './shared/Logger';
import httpContext from 'express-http-context';

import path from 'path';
import BaseRouter from './routes';

// Init express
const app = express();

// Add middleware/settings/routes to express.
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
app.use(morgan('combined', { stream: { write: message => logger.http(message) } }));

app.use(httpContext.middleware);

app.use(express.static(path.join(__dirname, 'public')));
app.use('/', BaseRouter);

const staticDir = path.join(__dirname, 'public');
app.use(express.static(staticDir));

// Export express instance
export default app;
