// Do not sort imports - module aliases must be added before other imports
import { addAliases } from 'module-alias';

// paths MUST also be defined in tsconfig.json
addAliases({
    '@src': __dirname,
    '@entities': __dirname + '/entities',
    '@logic': __dirname + '/logic',
    '@shared': __dirname + '/shared',
});

import app from './app';
import httpGracefulShutdown from 'http-graceful-shutdown';

// Start the server
const port = Number(process.env.PORT || 3000);
const server = app.listen(port, () => {
    console.log('Express server listening on port ' + port);
});

// Graceful shutdown on SIGINT/SIGTERM
httpGracefulShutdown(server, { timeout: 5000 });
