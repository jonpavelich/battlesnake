import { Router } from 'express';
import SnakeRouter from './Snake';
import LogRouter from './Logs';

// Init router and path
const router = Router();

// Add sub-routes

router.use('/', SnakeRouter);
router.use('/', LogRouter);
/* ... */

// Export the base-router
export default router;
