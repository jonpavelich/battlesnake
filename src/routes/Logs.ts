import { Request, Response, Router } from 'express';
import { logger } from '@shared';
import readline from 'readline';
import fs from 'fs';

// Init shared
const router = Router();

/******************************************************************************
 *                      Logs (read the snake's mind) - "POST /logs"
 * Read back recorded logs for analysis and training purposes
 ******************************************************************************/
router.get('/logs', async (req: Request, res: Response) => {
    try {
        // Check the request has a valid auth token
        const reqToken: string = req.body.auth;
        const tokens: string[] = JSON.parse(process.env.LOG_TOKENS || '[]');
        if (!reqToken || !tokens.includes(reqToken)) {
            return res.status(401).send('Not authorized');
        }

        // Filter properties to limit the returned logs
        const game = req.body.game;
        const snake = req.body.snake;
        const logPath = 'logs/combined.log';

        // Send an empty array if the log file doesn't exist
        if (!fs.existsSync(logPath)) {
            return res.send('[]');
        }

        // Read the log file line-by-line, keeping only selected lines in memory
        const lines: string[] = [];
        const lineReader = readline.createInterface({
            input: fs.createReadStream(logPath),
        });
        for await (const line of lineReader) {
            const logline = JSON.parse(line);
            if (game && game !== logline.game) {
                continue;
            }
            if (snake && snake !== logline.snake) {
                continue;
            }
            lines.push(line);
        }

        // Send the response as JSON
        let result = '[';
        if (lines.length > 0) {
            result = result.concat(lines.join(',\n'), '\n');
        }
        result = result.concat(']');
        return res.send(result);
    } catch (err) {
        logger.error(err.message, err);
        return res.status(400).json({
            error: err.message,
        });
    }
});

export default router;
