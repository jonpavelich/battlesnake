import { Request, Response, Router } from 'express';
import { MoveRequest } from '@entities';
import { logger } from '@shared';
import { getMove } from '@logic';
import httpContext from 'express-http-context';

// Init shared
const router = Router();

/******************************************************************************
 *                      Get Ping Response - "GET /ping"
 ******************************************************************************/

router.get('/ping', async (req: Request, res: Response) => {
    try {
        return res.status(200).end();
    } catch (err) {
        logger.error(err.message, err);
        return res.status(400).json({
            error: err.message,
        });
    }
});

/******************************************************************************
 *                      Start (Wake the Snake) - "GET /start"
 * Importantly this returns the color, head, and tail of the snake
 ******************************************************************************/

router.post('/start', async (req: Request, res: Response) => {
    try {
        return res.status(200).json({
            color: '#FFD700',
            headType: 'beluga',
            tailType: 'bolt',
        });
    } catch (err) {
        logger.error(err.message, err);
        return res.status(400).json({
            error: err.message,
        });
    }
});

/******************************************************************************
 *                      End (Put the snake to sleep) - "POST /end"
 * No action here - this snake is stateless
 ******************************************************************************/

router.post('/end', async (req: Request, res: Response) => {
    try {
        // TODO: Log this somehow? Could be useful for testing to know how many we win / lose
        return res.status(200).end();
    } catch (err) {
        logger.error(err.message, err);
        return res.status(400).json({
            error: err.message,
        });
    }
});

/******************************************************************************
 *                      Move (money moves) - "POST /move"
 * Do smart snake things in here
 ******************************************************************************/

router.post('/move', async (req: Request, res: Response) => {
    try {
        // TODO: Perform a parameter check on the move object (so if it breaks, we know why)

        const { game, turn, board, you } = req.body;
        const moveRequest = new MoveRequest({ game, turn, board, you });

        // Set the game and snake IDs for this request's context (used by Logger)
        httpContext.set('game', moveRequest.game.id);
        httpContext.set('snake', moveRequest.you.id);

        // Log the request for debugging later
        logger.info({ message: 'Received move request', request: req.body });

        // Process the request to get a response
        const moveResponse = getMove(moveRequest);
        return res.status(200).json(moveResponse);
    } catch (err) {
        logger.error(err.message, err);
        return res.status(400).json({
            error: err.message,
        });
    }
});

export default router;
