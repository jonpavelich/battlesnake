/* eslint-disable */
import { logger } from '@shared';

before(() => {
    logger.warn('Muting log output before test run');
    logger.silent = true;
});

after(() => {
    logger.silent = false;
    logger.warn('Unmuting log output after test run');
});
