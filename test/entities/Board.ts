import chai, { expect } from 'chai';
import { Board, BattleSnake } from '@entities';

const sampleData = {
    height: 15,
    width: 15,
    food: [
        {
            x: 3,
            y: 5,
        },
        {
            x: 4,
            y: 6,
        },
    ],
    snakes: [
        {
            id: 'snake-id-string',
            name: 'Sneky Snek',
            health: 90,
            body: [
                {
                    x: 1,
                    y: 3,
                },
                {
                    x: 1,
                    y: 4,
                },
            ],
        },
    ],
};

describe('initializes', () => {
    it('should create a new board', () => {
        const board = new Board({ ...sampleData });
        expect(board.height).to.eql(15);
        expect(board.width).to.eql(15);
        expect(board.food).to.have.deep.members([
            {
                x: 3,
                y: 5,
            },
            {
                x: 4,
                y: 6,
            },
        ]);
        expect(board.snakes.length).to.eql(1);
        expect(board.snakes[0]).to.be.instanceOf(BattleSnake);
        expect(board.snakes[0].body).to.eql([
            {
                x: 1,
                y: 3,
            },
            {
                x: 1,
                y: 4,
            },
        ]);
    });

    it('should detect inbounds correctly', () => {
        const board = new Board({ ...sampleData });
        /* base case */
        expect(board.isInbounds({ x: 3, y: 2 })).to.be.true;
        /* test negatives (too small) */
        expect(board.isInbounds({ x: -1, y: 1 })).to.be.false;
        expect(board.isInbounds({ x: 1, y: -1 })).to.be.false;
        /* test oversize (too large) */
        expect(board.isInbounds({ x: 18, y: 1 })).to.be.false;
        expect(board.isInbounds({ x: 1, y: 18 })).to.be.false;
        /* boundary cases (too large) */
        expect(board.isInbounds({ x: 1, y: 15 })).to.be.false;
        expect(board.isInbounds({ x: 15, y: 2 })).to.be.false;
        /* boundary cases (just right) */
        expect(board.isInbounds({ x: 1, y: 14 })).to.be.true;
        expect(board.isInbounds({ x: 14, y: 2 })).to.be.true;
    });

    it('should get surroundings correctly', () => {
        const board = new Board({ ...sampleData });
        /* base case */
        expect(board.getSurrounding({ x: 3, y: 3 })).to.eql([
            { x: 2, y: 2 },
            { x: 2, y: 3 },
            { x: 2, y: 4 },
            { x: 3, y: 2 },
            { x: 3, y: 4 },
            { x: 4, y: 2 },
            { x: 4, y: 3 },
            { x: 4, y: 4 },
        ]);
        /* in corner */
        expect(board.getSurrounding({ x: 0, y: 0 })).to.eql([
            { x: 0, y: 1 },
            { x: 1, y: 0 },
            { x: 1, y: 1 },
        ]);
        /* on x=0 wall */
        expect(board.getSurrounding({ x: 0, y: 1 })).to.eql([
            { x: 0, y: 0 },
            { x: 0, y: 2 },
            { x: 1, y: 0 },
            { x: 1, y: 1 },
            { x: 1, y: 2 },
        ]);
        /* on x=max wall */
        expect(board.getSurrounding({ x: 14, y: 1 })).to.eql([
            { x: 13, y: 0 },
            { x: 13, y: 1 },
            { x: 13, y: 2 },
            { x: 14, y: 0 },
            { x: 14, y: 2 },
        ]);
        /* on x=max wall */
        expect(board.getSurrounding({ x: 1, y: 14 })).to.eql([
            { x: 0, y: 13 },
            { x: 0, y: 14 },
            { x: 1, y: 13 },
            { x: 2, y: 13 },
            { x: 2, y: 14 },
        ]);
    });
});
