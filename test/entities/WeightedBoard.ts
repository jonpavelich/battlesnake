import mocha from 'mocha';
import chai, { expect } from 'chai';
import { Board, BattleSnake, WeightedBoard } from '@entities';

const sampleData = {
    height: 5,
    width: 5,
    food: [
        {
            x: 3,
            y: 5,
        },
        {
            x: 4,
            y: 6,
        },
    ],
    snakes: [
        {
            id: 'snake-id-string-1',
            name: 'Sneky Snek',
            health: 90,
            body: [
                {
                    x: 1,
                    y: 3,
                },
                {
                    x: 1,
                    y: 4,
                },
            ],
        },
        {
            id: 'snake-id-string-2',
            name: 'Not So Sneky Snek',
            health: 90,
            body: [
                {
                    x: 4,
                    y: 3,
                },
                {
                    x: 4,
                    y: 4,
                },
                {
                    x: 4,
                    y: 5,
                },
            ],
        },
    ],
};

const sampleWeights = [
    [1, 2, 0, 0, 0],
    [2, 1, 2, 0, 0],
    [0, 2, 1, 2, 0],
    [0, 0, 2, 1, 2],
    [0, 0, 0, 2, 1],
];

describe('WeightedBoard', () => {
    it('initialize a new weighted board with no given weights', () => {
        const board = new WeightedBoard({ ...sampleData });
        expect(board.height).to.eql(5);
        expect(board.width).to.eql(5);
        expect(board.food).to.have.deep.members([
            {
                x: 3,
                y: 5,
            },
            {
                x: 4,
                y: 6,
            },
        ]);
        expect(board.snakes.length).to.eql(2);
        expect(board.snakes[0]).to.be.instanceOf(BattleSnake);
        expect(board.weights).to.eql([
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0],
        ]);
    });

    it('initialize a new weighted board with given weights', () => {
        const board = new WeightedBoard({ ...sampleData, weights: sampleWeights });
        expect(board.height).to.eql(5);
        expect(board.width).to.eql(5);
        expect(board.food).to.have.deep.members([
            {
                x: 3,
                y: 5,
            },
            {
                x: 4,
                y: 6,
            },
        ]);
        expect(board.snakes.length).to.eql(2);
        expect(board.snakes[0]).to.be.instanceOf(BattleSnake);
        expect(board.weights).to.eql([
            [1, 2, 0, 0, 0],
            [2, 1, 2, 0, 0],
            [0, 2, 1, 2, 0],
            [0, 0, 2, 1, 2],
            [0, 0, 0, 2, 1],
        ]);
    });
});
