import chai, { expect } from 'chai';
import { BattleSnake } from '@entities';

const sampleData = {
    id: 'snake-id-string',
    name: 'Sneky Snek',
    health: 90,
    body: [
        {
            x: 1,
            y: 3,
        },
        {
            x: 1,
            y: 4,
        },
    ],
};

describe('initializes', () => {
    it('should create a new snake', () => {
        const snake = new BattleSnake({ ...sampleData });
        expect(snake.id).to.eql('snake-id-string');
        expect(snake.name).to.eql('Sneky Snek');
        expect(snake.health).to.eql(90);
        expect(snake.body).to.have.deep.members([
            {
                x: 1,
                y: 3,
            },
            {
                x: 1,
                y: 4,
            },
        ]);
    });
});

describe('acessors', () => {
    const snake = new BattleSnake({ ...sampleData });

    it("should return the snake's length", () => {
        expect(snake.length()).to.eql(2);
    });
    it("should return the snake's head", () => {
        expect(snake.head()).to.eql({ x: 1, y: 3 });
    });
    it("should return the snake's tail", () => {
        expect(snake.tail()).to.eql({ x: 1, y: 4 });
    });
});
