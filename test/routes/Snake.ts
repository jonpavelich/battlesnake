import chai from 'chai';
import chaiHttp from 'chai-http';
chai.use(chaiHttp);

import app from '@src/app';

describe('GET /ping', () => {
    it('should return status 200', () => {
        return chai
            .request(app)
            .get('/ping')
            .then(res => {
                chai.expect(res.status).to.eql(200);
            });
    });
});

describe('POST /start', () => {
    it('should return snake settings', () => {
        return chai
            .request(app)
            .post('/start')
            .set('content-type', 'application/json')
            .send({
                game: {
                    id: 'game-id-string',
                },
                turn: 0,
                board: {
                    height: 15,
                    width: 15,
                    food: [],
                    snakes: [],
                },
                you: {
                    id: 'snake-id-string',
                    name: 'Sneky Snek',
                    health: 90,
                    body: [],
                },
            })
            .then(res => {
                chai.expect(res.status).to.eql(200);
                chai.expect(res.body).to.eql({
                    color: '#FFD700',
                    headType: 'beluga',
                    tailType: 'bolt',
                });
            });
    });
});

describe('POST /end', () => {
    it('should return status 200', () => {
        return chai
            .request(app)
            .post('/end')
            .then(res => {
                chai.expect(res.status).to.eql(200);
            });
    });
});

describe('POST /move base case', () => {
    it('should return a valid move', () => {
        return chai
            .request(app)
            .post('/move')
            .set('content-type', 'application/json')
            .send({
                game: {
                    id: 'game-id-string',
                },
                turn: 4,
                board: {
                    height: 15,
                    width: 15,
                    food: [{ x: 2, y: 5 }],
                    snakes: [
                        {
                            id: 'snake-id-string',
                            name: 'Sneky Snek',
                            health: 90,
                            body: [
                                {
                                    x: 1,
                                    y: 3,
                                },
                                {
                                    x: 1,
                                    y: 4,
                                },
                            ],
                        },
                        {
                            id: 'snake-id-string-2',
                            name: 'Not So Sneky Snek',
                            health: 90,
                            body: [
                                {
                                    x: 4,
                                    y: 3,
                                },
                                {
                                    x: 4,
                                    y: 4,
                                },
                                {
                                    x: 4,
                                    y: 5,
                                },
                            ],
                        },
                    ],
                },
                you: {
                    id: 'snake-id-string',
                    name: 'Sneky Snek',
                    health: 90,
                    body: [
                        {
                            x: 1,
                            y: 3,
                        },
                        {
                            x: 1,
                            y: 4,
                        },
                    ],
                },
            })
            .then(res => {
                chai.expect(res.status).to.eql(200);
                chai.expect(res.body.move).to.be.oneOf(['left', 'right', 'up', 'down']);
            });
    });
});

/* test cases including example move requests that have crashed the app in the past
   included here as integration tests to avoid app crashes in the future */
describe('POST /move integration tests addressing previous app crashes', () => {
    /* Test addresses issue where the path reconstruction ends prematurely,
   resulting in attemping to access an undefined resource*/
    it('should return a valid move without crashing', () => {
        return chai
            .request(app)
            .post('/move')
            .set('content-type', 'application/json')
            .send({
                game: {
                    id: '3cd262d1-280f-4500-bf44-5dcb36c47c07',
                },
                turn: 13,
                board: {
                    height: 11,
                    width: 11,
                    food: [
                        {
                            x: 9,
                            y: 9,
                        },
                        {
                            x: 9,
                            y: 8,
                        },
                        {
                            x: 9,
                            y: 7,
                        },
                    ],
                    snakes: [
                        {
                            id: 'gs_mSfCxcxg8XqrybgTXRc36MpX',
                            name: 'dumb-snakev04',
                            health: 90,
                            body: [
                                {
                                    x: 7,
                                    y: 8,
                                },
                                {
                                    x: 6,
                                    y: 8,
                                },
                                {
                                    x: 5,
                                    y: 8,
                                },
                                {
                                    x: 4,
                                    y: 8,
                                },
                                {
                                    x: 4,
                                    y: 9,
                                },
                            ],
                            shout: '',
                        },
                        {
                            id: 'gs_gW4rSR4TX8V4CYg3SfHbFhyT',
                            name: 'wesley snipes',
                            health: 94,
                            body: [
                                {
                                    x: 6,
                                    y: 7,
                                },
                                {
                                    x: 5,
                                    y: 7,
                                },
                                {
                                    x: 4,
                                    y: 7,
                                },
                                {
                                    x: 3,
                                    y: 7,
                                },
                                {
                                    x: 2,
                                    y: 7,
                                },
                            ],
                            shout: '',
                        },
                        {
                            id: 'gs_WVVBBVDrxyCQVgXc99XJftmD',
                            name: 'SneakySnek BETA',
                            health: 99,
                            body: [
                                {
                                    x: 10,
                                    y: 3,
                                },
                                {
                                    x: 9,
                                    y: 3,
                                },
                                {
                                    x: 8,
                                    y: 3,
                                },
                                {
                                    x: 8,
                                    y: 4,
                                },
                            ],
                            shout: '',
                        },
                    ],
                },
                you: {
                    id: 'gs_WVVBBVDrxyCQVgXc99XJftmD',
                    name: 'SneakySnek BETA',
                    health: 99,
                    body: [
                        {
                            x: 10,
                            y: 3,
                        },
                        {
                            x: 9,
                            y: 3,
                        },
                        {
                            x: 8,
                            y: 3,
                        },
                        {
                            x: 8,
                            y: 4,
                        },
                    ],
                    shout: '',
                },
            })
            .then(res => {
                chai.expect(res.status).to.eql(200);
                chai.expect(res.body.move).to.be.oneOf(['left', 'right', 'up', 'down']);
            });
    });
    /* test addresses a case where the AStar algorithm queue contained circular references
       resulting in an infinite loop state */
    it('should return a valid move without crashing', () => {
        return chai
            .request(app)
            .post('/move')
            .set('content-type', 'application/json')
            .send({
                game: {
                    id: '88a8c693-6052-447b-a905-80f4a89bc719',
                },
                turn: 3,
                board: {
                    height: 11,
                    width: 11,
                    food: [
                        {
                            x: 1,
                            y: 4,
                        },
                    ],
                    snakes: [
                        {
                            id: 'gs_HhDVHxbmkK9dDwMHhvbKSJ6C',
                            name: 'SneakySnek',
                            health: 97,
                            body: [
                                {
                                    x: 5,
                                    y: 6,
                                },
                                {
                                    x: 5,
                                    y: 7,
                                },
                                {
                                    x: 5,
                                    y: 8,
                                },
                            ],
                            shout: '',
                        },
                        {
                            id: 'gs_mqCDh3vPXhBvpFfxrpP9mBpV',
                            name: 'SneakySnek BETA',
                            health: 99,
                            body: [
                                {
                                    x: 9,
                                    y: 6,
                                },
                                {
                                    x: 9,
                                    y: 7,
                                },
                                {
                                    x: 9,
                                    y: 8,
                                },
                                {
                                    x: 9,
                                    y: 9,
                                },
                            ],
                            shout: '',
                        },
                    ],
                },
                you: {
                    id: 'gs_mqCDh3vPXhBvpFfxrpP9mBpV',
                    name: 'SneakySnek BETA',
                    health: 99,
                    body: [
                        {
                            x: 9,
                            y: 6,
                        },
                        {
                            x: 9,
                            y: 7,
                        },
                        {
                            x: 9,
                            y: 8,
                        },
                        {
                            x: 9,
                            y: 9,
                        },
                    ],
                    shout: '',
                },
            })
            .then(res => {
                chai.expect(res.status).to.eql(200);
                chai.expect(res.body.move).to.be.oneOf(['left', 'right', 'up', 'down']);
            });
    });
});
