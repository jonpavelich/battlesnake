/* eslint-disable */
var { removeSync, copySync } = require('fs-extra');
var { exec } = require('child_process');

// Remove current build
removeSync('./dist/');
// Copy front-end files
copySync('./src/public', './dist/public');
// Transpile the typescript files
exec('tsc --build tsconfig.prod.json', (error, stdout, stderr) => {
    console.log(stdout);
    if (error) {
        console.error(error);
        console.error(stderr);
        process.exit(-1);
    }
});
